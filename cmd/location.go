package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

// Use WAN address to obtain location data
func getLocation(app *Application) {

	res, err := http.Get("https://ipinfo.io/json")
	if err != nil {
		fmt.Println("Unable to automatically obtain location, please try again.")
		log.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("Error reading location from server: %v\n", err)
	}

	err = json.Unmarshal(body, &app.Config)
	if err != nil {
		log.Printf("Error reading JSON from server: %v", err)
	}

	loc := strings.Split(app.Config.Location, ",")
	app.Config.Latitude, app.Config.Longitude = loc[0], loc[1]
}

// Prompt user for coordinates
func getPreciseLocation(app *Application) {

	fmt.Print("\nEnter latitude: ")
	reader := bufio.NewReader(os.Stdin)
	input, err := reader.ReadString('\n')
	if err != nil {
		log.Println(err)
	}
	app.Config.Latitude = strings.TrimSuffix(input, "\n")

	fmt.Print("\nEnter longitude: ")
	input, err = reader.ReadString('\n')
	if err != nil {
		log.Println(err)
	}
	app.Config.Longitude = strings.TrimSuffix(input, "\n")
}

// Prompt user for zip code
func getZip(app *Application) {
	fmt.Print("\nEnter 5-digit zip code: ")
	_, err := fmt.Scanf("%s", &app.Config.Location)
	if err != nil {
		log.Println(err)
	}
}

// Prompt user for city
func getCity(app *Application) {
	fmt.Print("\nEnter city: ")
	_, err := fmt.Scanf("%s", &app.Config.Location)
	if err != nil {
		log.Println(err)
	}
}
