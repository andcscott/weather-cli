package main

import (
	"log"
	"os"

	pb "codeberg.org/andcscott/weather-cli/proto"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const version string = "1.0.2"

type Weather struct {
	Temp      float32 `json:"temp"`
	FeelsLike float32 `json:"feels_like"`
	HighTemp  float32 `json:"temp_max"`
	LowTemp   float32 `json:"temp_min"`
	Pressure  uint    `json:"pressure"`
	Humidity  uint    `json:"humidity"`
}

type Wind struct {
	Speed float32 `json:"speed"`
	Gust  float32 `json:"gust"`
}

type Forecast struct {
	Weather Weather `json:"main"`
	Wind    Wind    `json:"wind"`
}

type HistoricalForecast struct {
	Temp      float32 `json:"temp"`
	FeelsLike float32 `json:"feels_like"`
	Pressure  uint    `json:"pressure"`
	Humidity  uint    `json:"humidity"`
	Speed     float32 `json:"wind_speed"`
}

type HistoricalData struct {
	HistoricalForecast []HistoricalForecast `json:"data"`
}

type Date struct {
	Year  int32
	Month int32
	Day   int32
}

type Config struct {
	Units     string
	Location  string `json:"loc"`
	Longitude string `json:"lat"`
	Latitude  string `json:"lon"`
	Date      Date
	ApiKey    string
}

type Application struct {
	Forecast           Forecast
	HistoricalForecast HistoricalForecast
	Config             Config
	Version            string
	Client             pb.RouteGuideClient
}

func main() {

	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	// Load .env
	err = godotenv.Load()
	if err != nil {
		log.Fatalln(err)
	}

	// Read API_KEY from .env and create app
	cfg := Config{
		Units:  "imperial",
		ApiKey: os.Getenv("API_KEY"),
	}

	fcst := Forecast{}
	hFcst := HistoricalForecast{}
	fcst.Weather.Temp = -500.00
	hFcst.Temp = -500.00

	app := Application{
		Config:             cfg,
		Forecast:           fcst,
		HistoricalForecast: hFcst,
		Version:            version,
		Client:             pb.NewRouteGuideClient(conn),
	}
	mainMenu(&app)
}
