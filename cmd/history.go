package main

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	pb "codeberg.org/andcscott/weather-cli/proto"
)

// Get a date from the user and save it to the config
func getDate(app *Application) {
	var isValid bool
	for !isValid {
		fmt.Print("\nEnter 4-digit year: ")
		_, err := fmt.Scanf("%d", &app.Config.Date.Year)
		if err != nil {
			fmt.Println("Invalid year")
		}

		fmt.Print("\nEnter month (1-12): ")
		_, err = fmt.Scanf("%d", &app.Config.Date.Month)
		if err != nil {
			fmt.Println("Invalid month")
		}

		fmt.Print("\nEnter day (1-31): ")
		_, err = fmt.Scanf("%d", &app.Config.Date.Day)
		if err != nil {
			fmt.Println("Invalid day")
		} else {
			fmt.Print("\n")
			isValid = true
		}
	}
}

// Query SimplrWeather for date in Unix time
func getUnixTime(c pb.RouteGuideClient) int32 {
	var year, month, day int32
	res, err := c.GetUnixTime(context.Background(), &pb.Date{
		Year:  year,
		Month: month,
		Day:   day,
	})
	if err != nil {
		fmt.Printf("Error getting Unix time: %v\n", err)
	}
	return res.Unixtime
}

// Query SimplrWeather for historical data
func getHistoricalData(c pb.RouteGuideClient, app *Application) {

	var lat, lon int
	lat, _ = strconv.Atoi(app.Config.Latitude)
	lon, _ = strconv.Atoi(app.Config.Longitude)

	res, err := c.GetHistoricalData(context.Background(), &pb.LocationDate{
		Latitude:  int32(lat),
		Longitude: int32(lon),
		Year:      app.Config.Date.Year,
		Month:     app.Config.Date.Month,
		Day:       app.Config.Date.Day,
	})
	if err != nil {
		fmt.Printf("Error getting historical data: %v", err)
	}

	hData := HistoricalData{}
	err = json.Unmarshal([]byte(res.Data), &hData)
	if err != nil {
		fmt.Printf("Error reading data from server: %v", err)
	} else {
		app.HistoricalForecast.Temp = hData.HistoricalForecast[0].Temp
		app.HistoricalForecast.FeelsLike = hData.HistoricalForecast[0].FeelsLike
		app.HistoricalForecast.Pressure = hData.HistoricalForecast[0].Pressure
		app.HistoricalForecast.Humidity = hData.HistoricalForecast[0].Humidity
		app.HistoricalForecast.Speed = hData.HistoricalForecast[0].Speed
	}
	convertUnits(app)
}

// Convert historical data from standard to users preferred units
func convertUnits(app *Application) {
	units := app.Config.Units
	switch units {
	case "imperial":
		app.HistoricalForecast.Temp = convertKToF(app.HistoricalForecast.Temp)
		app.HistoricalForecast.FeelsLike = convertKToF(app.HistoricalForecast.FeelsLike)
		app.HistoricalForecast.Speed = convertMpsToMph(app.HistoricalForecast.Speed)
	case "metric":
		app.HistoricalForecast.Temp = convertKToC(app.HistoricalForecast.Temp)
		app.HistoricalForecast.FeelsLike = convertKToC(app.HistoricalForecast.FeelsLike)
	default:
		fmt.Println("\nUnable to convert units, temperature will default to Kelvin and speed will default to meters per second.")
	}
}

// Convert Kelvin to Fahrenheit
func convertKToF(temp float32) float32 {
	return (9/5)*(temp-273) + 32
}

// Convert Kelvin to Celsius
func convertKToC(temp float32) float32 {
	return temp - 273.15
}

func convertMpsToMph(speed float32) float32 {
	return speed * 2.236936
}
